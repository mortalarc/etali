import { IBatchArray } from './IBatchArray'
import { chunks, proxyArrayAccess } from './util'

abstract class BatchArray<T> implements IBatchArray<T> {

  public rawBatches: T[][]
  public batchSize: number

  // Always use proxy to create BatchArray so it has indexed access
  public constructor(content: T[], batchSize: number) {

    const numBatches = Math.floor((content.length - 1) / batchSize) + 1

    this.rawBatches = [...chunks(content, numBatches)]
    this.batchSize = batchSize

  }

  public * contentIterator() {
    let idx = 0
    for (const batch of this.rawBatches) {
      for (const item of batch) {
        yield [idx, item] as [number, T]
        ++idx
      }
    }
  }


  public contentIdx(batchIdx: number, itemIdx: number) {
    if (batchIdx < 0 || batchIdx >= this.length) return undefined
    if (itemIdx < 0 || itemIdx >= this.rawBatches[batchIdx].length) return undefined
    let totIdx = 0
    for (let idx = 0; idx < batchIdx; ++idx) totIdx += this.rawBatches[idx].length
    return totIdx + itemIdx
  }


  // Note this will always return at the end of a batch rather than beginning of next
  public batchIdx(contentIdx?: number) {
    if (contentIdx == null) return undefined
    let numItems = 0
    let [batchIdx, itemIdx] = [-1, -1]
    for (const [idx, batch] of this.rawBatches.entries()) {
      numItems += batch.length
      if (batchIdx === -1 && numItems > contentIdx) {
        batchIdx = idx
        itemIdx = contentIdx - (numItems - batch.length)
        break
      }
    }
    if (batchIdx === -1) return undefined
    return [batchIdx, itemIdx] as [number, number]
  }

  // Given content idxs, reproduces the idxs in [batchIdx, itemIdx] form.
  // Note that endBatchIdx is NOT the idx above the last batch, it IS the last batch idx.
  // This makes sense if [batchIdx, itemIdx] is considered a pair.
  public batchIdxs(begIdx: number, endIdx: number):
    [[number, number], [number, number]] {

    let numItems = 0
    let [begBatchIdx, begItemIdx, endBatchIdx, endItemIdx] = [-1, -1, -1, -1]
    for (const [idx, batch] of this.rawBatches.entries()) {
      numItems += batch.length
      if (begBatchIdx === -1 && numItems > begIdx) {
        begBatchIdx = idx
        begItemIdx = begIdx - (numItems - batch.length)
      }
      if (endBatchIdx === -1 && numItems >= endIdx) {
        endBatchIdx = idx
        endItemIdx = endIdx - (numItems - batch.length)
        if (begIdx === endIdx) {
          begBatchIdx = idx
          begItemIdx = endItemIdx
        }
        break
      }
    }

    // Move beg back a batch if it's at an edge
    while (begBatchIdx > 0 && begItemIdx === 0) {
      begBatchIdx -= 1
      begItemIdx = this.rawBatches[begBatchIdx].length
    }
    // Move end forward a batch if it's at an edge
    while (endBatchIdx < this.rawBatches.length - 1 && endItemIdx === this.rawBatches[endBatchIdx].length) {
      endBatchIdx += 1
      endItemIdx = 0
    }

    if (begBatchIdx === -1) {
      begBatchIdx = this.rawBatches.length - 1
      begItemIdx = this.rawBatches[begBatchIdx].length
    }
    if (endBatchIdx === -1) {
      endBatchIdx = this.rawBatches.length - 1
      endItemIdx = this.rawBatches[endBatchIdx].length
    }

    return [[begBatchIdx, begItemIdx], [endBatchIdx, endItemIdx]]

  }


  protected abstract clone(): IBatchArray<T>

  public abstract filterInPlace(callbackFcn: (value: T, index: number, array: T[]) => any): void
  public abstract spliceAll(...splices: [number, number, ...T[]][]): void


  // Array core

  public find(
    predicate: (value: T, index: number, obj: T[]) => boolean,
    thisArg?: any
  ): T | undefined {
    let totIndex = 0
    for (const batch of this.rawBatches) {
      const batchPred = (value: T, index: number) => predicate(value, index + totIndex, this)
      const search = batch.find(batchPred, thisArg)
      if (search != null) return search
      totIndex += batch.length
    }
    return undefined
  }
  public findIndex(
    predicate: (value: T, index: number, obj: T[]) => unknown,
    thisArg?: any
  ): number {
    let totIndex = 0
    for (const batch of this.rawBatches) {
      const batchPred = (value: T, index: number) => predicate(value, index + totIndex, this)
      const searchIdx = batch.findIndex(batchPred, thisArg)
      if (searchIdx >= 0) return totIndex + searchIdx
      totIndex += batch.length
    }
    return -1
  }

  public fill(value: T, start?: number, end?: number): this {

    if (this.rawBatches.length === 0) return this

    let startBatchIdx: [number, number] | undefined
    if (start == null || start < -this.length) startBatchIdx = [0, 0]
    else if (start >= 0) startBatchIdx = this.batchIdx(start)
    else startBatchIdx = this.batchIdx(this.length + start)
    if (startBatchIdx == null) return this

    let endBatchIdx: [number, number] | undefined
    if (end == null || end > this.length) endBatchIdx = [this.rawBatches.length, 0]
    else if (end >= 0) endBatchIdx = this.batchIdx(end)
    else endBatchIdx = this.batchIdx(this.length + end)
    if (endBatchIdx == null) return this

    if (startBatchIdx[0] > endBatchIdx[0]) return this

    if (startBatchIdx[0] === endBatchIdx[0]) {
      this.rawBatches[startBatchIdx[0]].fill(value, startBatchIdx[1], endBatchIdx[1])
    } else {
      let batch = this.rawBatches[startBatchIdx[0]]
      batch.fill(value, startBatchIdx[1])
      for (let batchIdx = startBatchIdx[0] + 1; batchIdx < endBatchIdx[0]; ++batchIdx) {
        batch = this.rawBatches[batchIdx]
        batch.fill(value)
      }
      batch = this.rawBatches[endBatchIdx[0]]
      batch.fill(value, undefined, endBatchIdx[1])
    }
    return this

  }

  public copyWithin(target: number, start: number, end?: number): this {

    if (this.rawBatches.length === 0) return this

    let startBatchIdx: [number, number] | undefined
    if (start < -this.length) startBatchIdx = [0, 0]
    else if (start >= 0) startBatchIdx = this.batchIdx(start)
    else startBatchIdx = this.batchIdx(this.length + start)
    if (startBatchIdx == null) return this

    let endBatchIdx: [number, number] | undefined
    if (end == null || end > this.length) endBatchIdx = [this.rawBatches.length, 0]
    else if (end >= 0) endBatchIdx = this.batchIdx(end)
    else endBatchIdx = this.batchIdx(this.length + end)
    if (endBatchIdx == null) return this

    if (startBatchIdx[0] > endBatchIdx[0]) return this

    let targetBatchIdx: [number, number] | undefined
    if (target < -this.length) targetBatchIdx = [0, 0]
    else if (target >= 0) targetBatchIdx = this.batchIdx(target)
    else targetBatchIdx = this.batchIdx(this.length + target)
    if (targetBatchIdx == null) return this
    if (startBatchIdx[0] === targetBatchIdx[0] && startBatchIdx[1] === targetBatchIdx[1]) return this

    if (
      startBatchIdx[0] > targetBatchIdx[0] ||
      (startBatchIdx[0] === targetBatchIdx[0] && startBatchIdx[1] > targetBatchIdx[1])
    ) {

      // If start > target, copy from beginning to end
      let [origBatch, origItem] = startBatchIdx
      let [copyBatch, copyItem] = targetBatchIdx
      while (origBatch < endBatchIdx[0] || origItem < endBatchIdx[1]) {
        this.rawBatches[copyBatch][copyItem] = this.rawBatches[origBatch][origItem]
        ++origItem
        if (origItem >= this.rawBatches[origBatch].length) {
          ++origBatch
          origItem = 0
        }
        ++copyItem
        if (copyItem >= this.rawBatches[copyBatch].length) {
          ++copyBatch
          copyItem = 0
        }
      }

    } else {

      // If start < target, cut range if needed...
      const realContentStart = this.contentIdx(...startBatchIdx) as number
      const realContentEnd = this.contentIdx(...endBatchIdx) as number
      const realContentTarget = this.contentIdx(...targetBatchIdx) as number
      const lastTarget = realContentTarget + realContentEnd - realContentStart
      let origBatchIdx = endBatchIdx
      let copyBatchIdx = this.batchIdx(lastTarget)
      if (copyBatchIdx == null) {
        const goodContentEnd = realContentStart + this.length - realContentTarget
        origBatchIdx = this.batchIdx(goodContentEnd) as [number, number]
        copyBatchIdx = [this.rawBatches.length, 0]
      }

      // ...and copy end to beginning
      let [origBatch, origItem] = origBatchIdx
      let [copyBatch, copyItem] = copyBatchIdx
      while (origBatch > startBatchIdx[0] || origItem > startBatchIdx[1]) {
        --origItem
        if (origItem < 0) {
          --origBatch
          origItem = this.rawBatches[origBatch].length - 1
        }
        --copyItem
        if (copyItem < 0) {
          --copyBatch
          copyItem = this.rawBatches[copyBatch].length - 1
        }
        this.rawBatches[copyBatch][copyItem] = this.rawBatches[origBatch][origItem]
      }
    }

    return this

  }


  // Array iterable

  public *[Symbol.iterator](): IterableIterator<T> {
    for (const batch of this.rawBatches) {
      for (const item of batch) yield item
    }
  }
  public *entries(): IterableIterator<[number, T]> {
    let idx = 0
    for (const iter of this) {
      yield [idx, iter]
      idx += 1
    }
  }
  public *keys(): IterableIterator<number> {
    for (const [idx,] of this.entries()) {
      yield idx
    }
  }
  public *values(): IterableIterator<T> {
    yield* this
  }


  // Array include

  public includes(searchElement: T, fromIndex?: number): boolean {
    if (fromIndex == null) {
      return this.rawBatches.some(batch => batch.includes(searchElement))
    } else {
      const startBatchIdx = this.batchIdx(fromIndex)
      if (startBatchIdx == null) return false
      if (this.rawBatches[startBatchIdx[0]].includes(searchElement, startBatchIdx[1])) return true
      for (let batchIdx = startBatchIdx[0] + 1; batchIdx < this.rawBatches.length; ++batchIdx) {
        if (this.rawBatches[batchIdx].includes(searchElement)) return true
      }
      return false
    }
  }


  // Array es5

  public get length() {
    return this.rawBatches.reduce((num, batch) => num + batch.length, 0)
  }
  public toString(): string { return this.rawBatches.toString() }
  public toLocaleString(): string { return this.rawBatches.toLocaleString() }

  public concat(...items: IBatchArray<T>[]): IBatchArray<T> {
    // Note: this will not resize any raw batches
    const output = this.clone()
    for (const item of items) {
      if (item.rawBatches != null) output.rawBatches = output.rawBatches.concat(item.rawBatches)
    }
    return output
  }

  public join(separator?: string): string {
    return this.rawBatches.map(batch => batch.join(separator)).join(separator)
  }

  public reverse(): IBatchArray<T> {
    const output = this.clone()
    output.rawBatches = output.rawBatches.reverse().map(batch => batch.reverse())
    return output
  }

  public slice(start?: number, end?: number): T[] {
    const output: T[] = []
    if (start == null && end == null) {
      for (const batch of this.rawBatches) output.push(...batch)
    } else if (end == null) {
      if (start as number >= this.length) return []
      let begBatchIdx = this.batchIdx(start)
      if (begBatchIdx == null) begBatchIdx = [0, 0]
      output.push(...this.rawBatches[begBatchIdx[0]].slice(begBatchIdx[1]))
      for (let i = begBatchIdx[0] + 1; i < this.rawBatches.length; ++i)
        output.push(...this.rawBatches[i])
    } else if (start == null) {
      if (end as number <= 0) return []
      let endBatchIdx = this.batchIdx(end)
      if (endBatchIdx == null) {
        const last = this.rawBatches.length - 1
        endBatchIdx = [last, this.rawBatches[last].length]
      }
      for (let i = 0; i < endBatchIdx[0] + 1 && i < this.rawBatches.length; ++i)
        output.push(...this.rawBatches[i])
      output.push(...this.rawBatches[endBatchIdx[0]].slice(0, endBatchIdx[1]))
    } else {
      if (start as number >= this.length) return []
      if (end as number <= 0) return []
      let begBatchIdx = this.batchIdx(start)
      if (begBatchIdx == null) begBatchIdx = [0, 0]
      let endBatchIdx = this.batchIdx(end)
      if (endBatchIdx == null) {
        const last = this.rawBatches.length - 1
        endBatchIdx = [last, this.rawBatches[last].length]
      }
      if (begBatchIdx[0] === endBatchIdx[0]) {
        output.push(...this.rawBatches[begBatchIdx[0]].slice(begBatchIdx[1], endBatchIdx[1]))
      } else {
        output.push(...this.rawBatches[begBatchIdx[0]].slice(begBatchIdx[1]))
        for (let i = begBatchIdx[0] + 1; i < endBatchIdx[0] && i < this.rawBatches.length; ++i)
          output.push(...this.rawBatches[i])
        output.push(...this.rawBatches[endBatchIdx[0]].slice(0, endBatchIdx[1]))
      }
    }
    return output
  }

  public sort(compareFn?: (a: T, b: T) => number): this {
    // This is hugely memory heavy and may be super slow... but I don't want to make anything fancy for now
    const sortedBatches = ([] as T[]).concat(...this.rawBatches).sort(compareFn)
    let idx = 0
    for (const batch of this.rawBatches) {
      for (let itemIdx = 0; itemIdx < batch.length; ++itemIdx) {
        batch[itemIdx] = sortedBatches[idx]
        idx += 1
      }
    }
    return this
  }

  public indexOf(searchElement: T, fromIndex?: number): number {
    if (fromIndex == null) {
      return this.findIndex(val => val === searchElement)
    } else {
      const startBatchIdx = this.batchIdx(fromIndex)
      if (startBatchIdx == null) return -1
      let batchTot = 0
      for (let batchIdx = 0; batchIdx < startBatchIdx[0]; ++batchIdx) {
        batchTot += this.rawBatches[batchIdx].length
      }
      let trial = this.rawBatches[startBatchIdx[0]].indexOf(searchElement, startBatchIdx[1])
      if (trial !== -1) return batchTot + trial
      batchTot += this.rawBatches[startBatchIdx[0]].length
      for (let batchIdx = startBatchIdx[0] + 1; batchIdx < this.rawBatches.length; ++batchIdx) {
        trial = this.rawBatches[batchIdx].indexOf(searchElement)
        if (trial !== -1) return batchTot + trial
        batchTot += this.rawBatches[batchIdx].length
      }
      return -1
    }
  }

  public lastIndexOf(searchElement: T, fromIndex?: number): number {
    if (this.rawBatches.length === 0) return -1
    let startBatchIdx: [number, number] | undefined
    if (fromIndex) {
      startBatchIdx = this.batchIdx(fromIndex)
      if (startBatchIdx == null) return -1
    } else {
      startBatchIdx = [
        this.rawBatches.length - 1,
        this.rawBatches[this.rawBatches.length-1].length
      ]
    }
    let batchTot = 0
    for (let batchIdx = this.rawBatches.length-1; batchIdx > startBatchIdx[0]; --batchIdx) {
      batchTot += this.rawBatches[batchIdx].length
    }
    let trial = this.rawBatches[startBatchIdx[0]].lastIndexOf(searchElement, startBatchIdx[1])
    batchTot += this.rawBatches[startBatchIdx[0]].length
    if (trial !== -1) return this.length - batchTot + trial
    for (let batchIdx = startBatchIdx[0] - 1; batchIdx >= 0; --batchIdx) {
      trial = this.rawBatches[batchIdx].lastIndexOf(searchElement)
      batchTot += this.rawBatches[batchIdx].length
      if (trial !== -1) return this.length - batchTot + trial
    }
    return -1
  }

  public every(predicate: (value: T, index: number, array: T[]) => unknown, thisArg?: any): boolean {
    return this.rawBatches.every(batch => batch.every(predicate, thisArg))
  }
  public some(predicate: (value: T, index: number, array: T[]) => unknown, thisArg?: any): boolean {
    return this.rawBatches.some(batch => batch.some(predicate, thisArg))
  }

  public forEach(callbackfn: (value: T, index: number, array: T[]) => void, thisArg?: any): void {
    this.rawBatches.forEach(batch => batch.forEach(callbackfn, thisArg), thisArg)
  }

  // TODO: Return BatchArray<U> instead
  public map<U>(callbackfn: (value: T, index: number, array: T[]) => U, thisArg?: any): U[] {
    const output = []
    for (const batch of this.rawBatches) output.push(...batch.map(callbackfn, thisArg))
    return output
  }
  // TODO: Return BatchArray<U> instead
  public filter(predicate: (value: T, index: number, array: T[]) => boolean, thisArg?: any): T[] {
    const output = []
    for (const batch of this.rawBatches) output.push(...batch.filter(predicate, thisArg))
    return output
  }

  public flatMap<U>(callback: (value: T, index: number, array: T[]) => U[]): U[] {
    return Array.prototype.concat(...this.map(callback))
  }
  public flat<A, D extends number = 1>(this: A, depth?: D): FlatArray<A, D>[] {
    return Array.prototype.flat(depth)
  }

  public reduce<U>(callbackfn: (accumulator: U, currentValue: T, index: number, array: T[]) => U, initialValue?: U): U {
    let reduction
    if (initialValue != null) {
      reduction = initialValue
      for (const batch of this.rawBatches) reduction = batch.reduce<U>(callbackfn, reduction)
    } else {
      const callbackfnT = callbackfn as unknown as (accumulator: T, currentValue: T, index: number, array: T[]) => T
      reduction = this.rawBatches[0].reduce(callbackfnT) as unknown as U
      for (const batch of this.rawBatches.slice(1)) {
        reduction = batch.reduce(callbackfnT, reduction as unknown as T) as unknown as U
      }
    }
    return reduction
  }
  public reduceRight<U>(
    callbackfn: (accumulator: U, currentValue: T, index: number, array: T[]) => U,
    initialValue?: U
  ): U {
    let reduction
    if (initialValue != null) {
      reduction = initialValue
      for (const batch of this.rawBatches.reverse()) reduction = batch.reduceRight<U>(callbackfn, reduction)
    } else {
      const callbackfnT = callbackfn as unknown as (accumulator: T, currentValue: T, index: number, array: T[]) => T
      reduction = this.rawBatches[this.rawBatches.length - 1].reduceRight(callbackfnT) as unknown as U
      for (const batch of this.rawBatches.reverse().slice(1)) {
        reduction = batch.reduceRight(callbackfnT, reduction as unknown as T) as unknown as U
      }
    }
    return reduction
  }

  // This indexed access WILL NOT WORK without applying ArrayAccessProxy
  [n: number]: T

  at(n: number): T | undefined {
    return (n >= 0 && n < this.length) ? this[n] : undefined
  }


  // Array wellknown

  public [Symbol.unscopables]() { return {
    copyWithin: false,
    entries: false,
    fill: false,
    find: false,
    findIndex: false,
    keys: false,
    values: false
  } }


  // Array abstract

  public abstract pop(): T | undefined
  public abstract push(...items: T[]): number
  public abstract shift(): T | undefined
  public abstract splice(start: number, deleteCount: number, ...items: T[]): T[]
  public abstract unshift(...items: T[]): number

}



/*
  ImmutBatchArray; throws if mutation function called
*/
class _ImmutBatchArray<T> extends BatchArray<T> {

  private throwMut(methodName: string) {
    throw Error('Cannot use ' + methodName + ' in ImmutableBatchArray')
  }

  protected clone(): ImmutBatchArray<T> {
    const copy = ImmutBatchArray<T>([], this.batchSize)
    copy.rawBatches = this.rawBatches.map(batch => batch.slice())
    return copy
  }

  public filterInPlace(): void { this.throwMut('filterInPlace') }
  public spliceAll(): void { this.throwMut('spliceAll') }

  public pop(): T | undefined { this.throwMut('pop'); return undefined }
  public push(): number { this.throwMut('push'); return -1 }
  public shift(): T | undefined { this.throwMut('shift'); return undefined }
  public splice(): T[] { this.throwMut('splice'); return [] }
  public unshift(): number { this.throwMut('unshift'); return -1 }

}

type ImmutBatchArray<T> = _ImmutBatchArray<T>

function ImmutBatchArray<T>(content: T[], batchSize: number) {
  return proxyArrayAccess<T, ImmutBatchArray<T>>(
    new _ImmutBatchArray(content, batchSize)
  )
}


export { BatchArray, ImmutBatchArray }
