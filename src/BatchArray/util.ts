import { IBatchArray } from './IBatchArray'


function* chunks<T>(arr: T[], numChunks: number) {
  const avgFill = Math.floor(arr.length / numChunks)
  const remainder = arr.length - avgFill * numChunks
  let sliceIdx = 0
  for (let i = 0; i < remainder; ++i) {
    yield arr.slice(sliceIdx, sliceIdx + avgFill + 1)
    sliceIdx += avgFill + 1
  }
  for (let i = remainder; i < numChunks; ++i) {
    yield arr.slice(sliceIdx, sliceIdx + avgFill)
    sliceIdx += avgFill
  }
}

function proxyArrayAccess<T, A extends IBatchArray<T>>(arr: A) {
  const handler: ProxyHandler<A> = {
    get(target, prop) {
      if (!['number', 'string'].includes(typeof prop)) return (target[prop as any])
      const numProp = Number(prop)
      if (isNaN(numProp)) return (target[prop as any])
      if (numProp < 0 || numProp >= target.length) return undefined
      const batchIdx = target.batchIdx(numProp)
      if (batchIdx == null) return undefined
      return target.rawBatches[batchIdx[0]][batchIdx[1]]
    },
    set(target, prop, val) {
      if (!['number', 'string'].includes(typeof prop)) return (target[prop as any])
      const numProp = Number(prop)
      if (isNaN(numProp)) return (target[prop as any] = val)
      if (numProp < 0 || numProp >= target.length) return false
      const batchIdx = target.batchIdx(numProp)
      if (batchIdx == null) return false
      return (target.rawBatches[batchIdx[0]][batchIdx[1]] = val)
    }
  }
  return new Proxy(arr, handler)
}


export {
  chunks, proxyArrayAccess
}
