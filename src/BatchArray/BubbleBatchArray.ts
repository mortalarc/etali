import { BatchArray } from './BatchArray'
import { IBatchArray } from './IBatchArray'
import { chunks, proxyArrayAccess } from './util'


class _BubbleBatchArray<T> extends BatchArray<T> {

  // Suggested that minBatchSize <= maxBatchSize/2
  private readonly minBatchSize: number
  private readonly maxBatchSize: number
  private readonly optimalSize: number

  public constructor(
    content: T[], maxBatchSize: number,
    minBatchSize: number = maxBatchSize / 2
  ) {
    const optimalSize = minBatchSize + Math.round((maxBatchSize - minBatchSize) / 2)
    super(content, optimalSize)
    if (minBatchSize > maxBatchSize / 2) {
      console.warn(
        'Minimum batch size for BubbleBatchArray should be ' +
        'less than half maximum batch size; setting to max/2.'
      )
      this.minBatchSize = maxBatchSize / 2
    } else this.minBatchSize = minBatchSize
    this.maxBatchSize = maxBatchSize
    this.optimalSize = optimalSize
  }


  public filterInPlace(callbackFcn: (value: T, index: number, array: T[]) => any) {
    this.rawBatches.forEach((batch, idx) => {
      this.rawBatches[idx] = batch.filter(callbackFcn)
    })
    this.mergeSmallBatches()
  }


  public concat(...items: IBatchArray<T>[]): BubbleBatchArray<T> {
    let output: T[] = []
    output = output.concat(...this.rawBatches)
    for (const item of items) {
      if (item.rawBatches) output = output.concat(...item.rawBatches)
    }
    return BubbleBatchArray<T>(output, this.minBatchSize, this.maxBatchSize)
  }

  public splice(begIdx: number, numRemoved: number, ...addedItems: T[]) {
    const [rmedElems,affectedBatchRange] = this.spliceNoBubble(begIdx, numRemoved, ...addedItems)
    const mergeRange = this.mergeSmallBatches(affectedBatchRange)
    this.splitLargeBatches(mergeRange)
    return rmedElems
  }

  public spliceAll(...splices: [number, number, ...T[]][]) {

    if (splices.length === 0) return

    const sortedSplices = splices.sort((splice1, splice2) => splice1[0] - splice2[0])
    const reducedSplices = [sortedSplices[0]]
    let currentLast = 0
    for (let idx = 1; idx < sortedSplices.length; ++idx) {
      const newSplice = sortedSplices[idx]
      const prevSplice = reducedSplices[currentLast]
      const prevEnd = prevSplice[0] + prevSplice[1]
      if (newSplice[0] >= prevEnd) {
        reducedSplices.push(newSplice)
        ++currentLast
      } else prevSplice[1] = Math.max(prevSplice[1], prevEnd - prevSplice[0])
    }

    const affectedBatchRanges = []
    let prevRange: [number, number] | null = null
    for (const splice of reducedSplices.reverse()) {
      const [, batchRange] = this.spliceNoBubble(...splice)
      if (prevRange != null && batchRange[1] >= prevRange[0]) {
        if (batchRange[0] < prevRange[0]) prevRange[0] = batchRange[0]
      } else {
        affectedBatchRanges.push(batchRange)
        prevRange = batchRange
      }
    }
    let mergeRanges: [number, number][] = []
    for (const batchRange of affectedBatchRanges) {
      const mergeRange = this.mergeSmallBatches(batchRange)
      const overlapping = mergeRanges.filter(range => range[0] < mergeRange[1] && mergeRange[0] < range[1])
      if (overlapping.length > 0) {
        mergeRanges = mergeRanges.filter(range => !overlapping.includes(range))
        for (const r of overlapping) {
          if (r[0] < mergeRange[0]) mergeRange[0] = r[0]
          if (r[1] < mergeRange[1]) mergeRange[1] = r[1]
        }
      }
      mergeRanges.push(mergeRange)
    }
    for (const batchRange of mergeRanges) {
      this.splitLargeBatches(batchRange)
    }
  }

  private spliceNoBubble(begIdx: number, numRemoved: number, ...addedItems: T[]): [T[], [number, number]] {

    // Find where batches need to be spliced
    const [[begBatch, begItem], [endBatch, endItem]] =
      this.batchIdxs(begIdx, begIdx + numRemoved)

    // Distribute the addedItems
    const rmedItems = []
    if (begBatch === endBatch) {

      // Distribute in this one batch, obviously
      rmedItems.push(...this.rawBatches[begBatch].splice(begItem, numRemoved, ...addedItems))

    } else {

      // It might be better to equitably distribute this, but for now just put equal amounts in each batch
      const avgFill = Math.floor(addedItems.length / (endBatch - begBatch + 1))
      const remainder = addedItems.length - avgFill * (endBatch - begBatch + 1)

      let numAdded = 0
      for (const [batchIdx, batch] of this.rawBatches.slice(begBatch, endBatch + 1).entries()) {
        let fill = avgFill
        if (batchIdx < remainder) fill += 1
        const start = (batchIdx === 0) ? begItem : 0
        const end = (batchIdx === endBatch - begBatch) ? endItem : batch.length
        rmedItems.push(...batch.splice(
          start, end - start, ...addedItems.slice(numAdded, numAdded + fill)
        ))
        numAdded += fill
      }

    }

    return [rmedItems, [begBatch, endBatch + 1]]

  }


  private mergeSmallBatches(batchRange: [number, number] = [0, this.rawBatches.length]): [number, number] {

    if (batchRange[0] < 0 || batchRange[0] >= this.rawBatches.length) batchRange[0] = 0
    if (batchRange[1] <= 0 || batchRange[1] > this.rawBatches.length) batchRange[1] = this.rawBatches.length

    const merges: [number, number][] = []
    let idx = batchRange[0]
    while (idx < batchRange[1]) {

      let batch = this.rawBatches[idx]
      if (batch.length < this.minBatchSize) {

        const merge = [idx, idx+1] as [number, number]
        let mergeTot = batch.length
        ++idx
        while (idx < this.rawBatches.length) {
          batch = this.rawBatches[idx]
          if (batch.length > this.minBatchSize) break
          mergeTot += batch.length
          if (mergeTot > this.maxBatchSize) break
          merge[1] += 1
          ++idx
        }

        if (merge[1] - merge[0] > 1) merges.push(merge)

      } else {
        ++idx
      }
    }

    let mergedSoFar = 0
    for (const merge of merges) {
      const begMerge = merge[0] - mergedSoFar
      const numMerge = merge[1] - merge[0]
      this.rawBatches.splice(begMerge, numMerge, ([] as T[]).concat(
        ...this.rawBatches.slice(begMerge, begMerge + numMerge)
      ))
      mergedSoFar += numMerge - 1
    }

    return [batchRange[0], batchRange[1] - mergedSoFar]

  }

  private splitLargeBatches(batchRange: [number, number] = [0, this.rawBatches.length]) {

    const splitIdxs = [] as number[]
    for (let idx = batchRange[0]; idx < batchRange[1]; ++idx) {
      if (this.rawBatches[idx].length > this.maxBatchSize) splitIdxs.push(idx)
    }

    let splitSoFar = 0
    for (const idx of splitIdxs) {
      const splitIdx = idx + splitSoFar
      const batch = this.rawBatches[splitIdx]
      const numSplit = Math.round(batch.length / this.optimalSize)
      this.rawBatches.splice(splitIdx, 1, ...chunks(batch, numSplit))
      splitSoFar += numSplit - 1
    }

  }


  protected clone(): BubbleBatchArray<T> {
    const copy = BubbleBatchArray<T>([], this.maxBatchSize, this.minBatchSize)
    copy.rawBatches = this.rawBatches.map(batch => batch.slice())
    return copy
  }

  public pop(): T | undefined {
    while (this.rawBatches.length > 0) {
      const poppedEl = this.rawBatches[this.rawBatches.length - 1].pop()
      if (poppedEl != null) {
        if (this.rawBatches[this.rawBatches.length - 1].length === 0) this.rawBatches.pop()
        this.mergeSmallBatches([this.rawBatches.length - 2, this.rawBatches.length - 1])
        return poppedEl
      }
      else this.rawBatches.pop()
    }
    return undefined
  }
  public push(...items: T[]): number {
    let origEndBatchIdx = this.rawBatches.length - 1
    if (origEndBatchIdx < 0) origEndBatchIdx = 0
    if (
      this.rawBatches.length === 0 ||
      this.rawBatches[this.rawBatches.length-1].length >= this.batchSize
    ) this.rawBatches.push([])
    let elIdx = 0
    while (elIdx < items.length) {
      const endBatch = this.rawBatches[this.rawBatches.length - 1]
      const endBatchSpace = this.batchSize - endBatch.length
      if (items.length - elIdx <= endBatchSpace) {
        endBatch.push(...items.slice(elIdx))
      } else {
        endBatch.push(...items.slice(elIdx, elIdx + endBatchSpace))
        this.rawBatches.push([])
      }
      elIdx += endBatchSpace
    }
    this.splitLargeBatches([origEndBatchIdx, this.rawBatches.length])
    return this.length
  }
  public shift(): T | undefined {
    while (this.rawBatches.length > 0) {
      const shiftedEl = this.rawBatches[0].shift()
      if (shiftedEl != null) {
        if (this.rawBatches[0].length === 0) this.rawBatches.shift()
        this.mergeSmallBatches([0, 1])
        return shiftedEl
      }
      else this.rawBatches.shift()
    }
    return undefined
  }
  public unshift(...items: T[]): number {
    let origBegBatchIdx = 0
    if (
      this.rawBatches.length === 0 ||
      this.rawBatches[0].length >= this.batchSize
    ) {
      this.rawBatches.unshift([])
      origBegBatchIdx += 1
    }
    let elIdx = items.length
    while (elIdx > 0) {
      const begBatch = this.rawBatches[0]
      const begBatchSpace = this.batchSize - begBatch.length
      if (elIdx <= begBatchSpace) {
        begBatch.unshift(...items.slice(0, elIdx))
      } else {
        begBatch.unshift(...items.slice(elIdx - begBatchSpace, elIdx))
        this.rawBatches.unshift([])
        origBegBatchIdx += 1
      }
      elIdx -= begBatchSpace
    }
    this.splitLargeBatches([0, origBegBatchIdx + 1])
    return this.length
  }

}


type BubbleBatchArray<T> = _BubbleBatchArray<T>

function BubbleBatchArray<T>(
  content: T[], maxBatchSize: number, minBatchSize?: number
) {
  return proxyArrayAccess<T, BubbleBatchArray<T>>(
    new _BubbleBatchArray(content, maxBatchSize, minBatchSize)
  )
}

export { BubbleBatchArray as default }
