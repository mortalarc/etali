interface IBatchArray<T> extends Array<T> {

  rawBatches: T[][]
  batchSize: number

  contentIterator(): Generator<[number, T], void>

  contentIdx(batchIdx: number, itemIdx: number): number | undefined
  batchIdx(contentIdx: number): [number, number] | undefined

  // Given content idxs, reproduces the idxs in [batchIdx, itemIdx] form.
  // Note that endBatchIdx is NOT the idx above the last batch, it IS the last batch idx.
  // This makes sense if [batchIdx, itemIdx] is considered a pair.
  batchIdxs(begIdx: number, endIdx: number): [[number, number], [number, number]]

  filterInPlace(callbackFcn: (value: T, index: number, array: T[]) => any): void
  splice(begIdx: number, numRemoved: number, ...addedItems: T[]): T[]
  spliceAll(...splices: [number, number, ...T[]][]): void

}

export { type IBatchArray }
