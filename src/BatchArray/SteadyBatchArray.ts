import { BatchArray } from './BatchArray'
import { IBatchArray } from './IBatchArray'
import { proxyArrayAccess } from './util'


/*
  This is a batched array that tries to keep the content that initializes it
   in the same batches but does not try too hard to keep the batches similar size, otherwise.
*/
class _SteadyBatchArray<T> extends BatchArray<T> {

  public filterInPlace(callbackFcn: (value: T, index: number, array: T[]) => any) {
    this.rawBatches.forEach((batch, idx) => {
      this.rawBatches[idx] = batch.filter(callbackFcn)
    })
  }


  public concat(...items: IBatchArray<T>[]): SteadyBatchArray<T> {
    let output: T[] = []
    output = output.concat(...this.rawBatches)
    for (const item of items) {
      if (item.rawBatches) output = output.concat(...item.rawBatches)
    }
    return SteadyBatchArray<T>(output, this.batchSize)
  }


  public splice(begIdx: number, numRemoved: number, ...addedItems: T[]) {

    // Find where batches need to be spliced
    const [[begBatch, begItem], [endBatch, endItem]] =
      this.batchIdxs(begIdx, begIdx + numRemoved)

    // Distribute the addedItems
    if (begBatch === endBatch) {

      // Distribute in this one batch, obviously
      return this.rawBatches[begBatch].splice(begItem, numRemoved, ...addedItems)

    } else {

      // It might be better to equitably distribute this, but for now just put equal amounts in each batch
      const avgFill = Math.floor(addedItems.length / (endBatch - begBatch + 1))
      const remainder = addedItems.length - avgFill * (endBatch - begBatch + 1)

      let numAdded = 0
      const deletedEls = []
      for (const [batchIdx, batch] of this.rawBatches.slice(begBatch, endBatch + 1).entries()) {
        let fill = avgFill
        if (batchIdx < remainder) fill += 1
        const start = (batchIdx === 0) ? begItem : 0
        const end = (batchIdx === endBatch - begBatch) ? endItem : batch.length
        deletedEls.push(...batch.splice(
          start, end - start, ...addedItems.slice(numAdded, numAdded + fill)
        ))
        numAdded += fill
      }
      return deletedEls

    }

  }

  public spliceAll(...splices: [number, number, ...T[]][]) {
    splices.forEach(splice => this.splice(...splice))
  }


  protected clone(): SteadyBatchArray<T> {
    const copy = SteadyBatchArray<T>([], this.batchSize)
    copy.rawBatches = this.rawBatches.map(batch => batch.slice())
    return copy
  }

  public pop(): T | undefined {
    while (this.rawBatches.length > 0) {
      const poppedEl = this.rawBatches[this.rawBatches.length - 1].pop()
      if (poppedEl != null) {
        if (this.rawBatches[this.rawBatches.length - 1].length === 0) this.rawBatches.pop()
        return poppedEl
      }
      else this.rawBatches.pop()
    }
    return undefined
  }
  public push(...items: T[]): number {
    if (
      this.rawBatches.length === 0 ||
      this.rawBatches[this.rawBatches.length-1].length >= this.batchSize
    ) this.rawBatches.push([])
    let elIdx = 0
    while (elIdx < items.length) {
      const endBatch = this.rawBatches[this.rawBatches.length - 1]
      const endBatchSpace = this.batchSize - endBatch.length
      if (items.length - elIdx <= endBatchSpace) {
        endBatch.push(...items.slice(elIdx))
      } else {
        endBatch.push(...items.slice(elIdx, elIdx + endBatchSpace))
        this.rawBatches.push([])
      }
      elIdx += endBatchSpace
    }
    return this.length
  }
  public shift(): T | undefined {
    while (this.rawBatches.length > 0) {
      const shiftedEl = this.rawBatches[0].shift()
      if (shiftedEl != null) {
        if (this.rawBatches[0].length === 0) this.rawBatches.shift()
        return shiftedEl
      }
      else this.rawBatches.shift()
    }
    return undefined
  }
  public unshift(...items: T[]): number {
    if (
      this.rawBatches.length === 0 ||
      this.rawBatches[0].length >= this.batchSize
    ) this.rawBatches.unshift([])
    let elIdx = items.length
    while (elIdx > 0) {
      const begBatch = this.rawBatches[0]
      const begBatchSpace = this.batchSize - begBatch.length
      if (elIdx <= begBatchSpace) {
        begBatch.unshift(...items.slice(0, elIdx))
      } else {
        begBatch.unshift(...items.slice(elIdx - begBatchSpace, elIdx))
        this.rawBatches.unshift([])
      }
      elIdx -= begBatchSpace
    }
    return this.length
  }

}


type SteadyBatchArray<T> = _SteadyBatchArray<T>

function SteadyBatchArray<T>(content: T[], batchSize: number) {
  return proxyArrayAccess<T, SteadyBatchArray<T>>(
    new _SteadyBatchArray(content, batchSize)
  )
}

export { SteadyBatchArray as default }
