/*
  lonlats should be expressed in degree coordinates and return is also in degrees
*/
export default function AverageLonLat(lonlats: Iterable<[number, number]>) {

  // sum_i cos(theta), sum_i sin(theta) cos(phi), sum_i sin(theta) sin(phi)
  let [sumC, sumSC, sumSS] = [0, 0, 0]
  for (const ll of lonlats) {
    const th = Math.PI / 2 - ll[1] * Math.PI / 180
    const ph = ll[0] * Math.PI / 180
    sumC += Math.cos(th)
    sumSC += Math.sin(th) * Math.cos(ph)
    sumSS += Math.sin(th) * Math.sin(ph)
  }

  /*
             sum_i sin(th_i) * sin(phi_i)
  tan(phi) = ----------------------------
             sum_i sin(th_i) * cos(phi_i)
  */
  let phi = Math.atan2(sumSS, sumSC)
  if (phi < 0) phi += 2 * Math.PI

  /*
               sqrt[ (sum_i sin(th_i) * cos(phi_i))^2 + (sum_i sin(th_i) * sin(phi_i))^2 ]
  tan(theta) = ---------------------------------------------------------------------------
                                          sum_i cos(th_i)
  */
  const theta = Math.abs(Math.atan(
    Math.sqrt(sumSC * sumSC + sumSS * sumSS) / sumC
  ))

  return [phi * 180 / Math.PI, 90 - theta * 180 / Math.PI] as [number, number]

}
