import { strict as assert } from 'assert'
import 'mocha'
import { FlattenRanges } from '../src'

type SimpleSpan<T> = [number, number, T]
type Span<T> = [number, number, T[]]

describe('FlattenRanges', function() {

  it('should return empty from empty', function() {
    assert.deepEqual(FlattenRanges([]), [])
  })

  it('should simply process disjoint ranges', function() {
    const input = [
      [1, 3, '1'],
      [5, 7, '2']
    ] as SimpleSpan<string>[]
    const expected = [
      [1, 3, ['1']],
      [5, 7, ['2']]
    ] as Span<string>[]
    assert.deepEqual(FlattenRanges(input), expected)
  })

  it('should flatten overlapping ranges', function() {
    const input = [
      [1, 5, '1'],
      [3, 7, '2']
    ] as SimpleSpan<string>[]
    const expected = [
      [1, 3, ['1']],
      [3, 5, ['1', '2']],
      [5, 7, ['2']]
    ] as Span<string>[]
    assert.deepEqual(FlattenRanges(input), expected)
  })

})
