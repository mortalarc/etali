import { strict as assert } from 'assert'
import 'mocha'
import { ImmutBatchArray } from '../src'

describe('BatchArray', function() {

  const batchSize = 5
  const data = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

  describe('batch behavior', function() {

    it('should return empty from empty', function() {
      const batchArray = ImmutBatchArray([], batchSize)
      assert.deepEqual(batchArray.rawBatches, [])
    })

    it('should batch by batch size', function() {
      const batchArray = ImmutBatchArray(data, batchSize)
      const expected = [
        [0, 1, 2, 3],
        [4, 5, 6, 7],
        [8, 9, 10]
      ]
      assert.deepEqual(batchArray.rawBatches, expected)
    })

    it('should accurately calculate content length', function() {
      const batchArray = ImmutBatchArray(data, batchSize)
      assert.equal(batchArray.length, 11)
    })

    it('should be iterable by contentIterator', function() {
      const batchArray = ImmutBatchArray(data, batchSize)
      const iter = batchArray.contentIterator()
      for (let idx = 0; idx < data.length; ++idx) {
        assert.deepEqual(iter.next().value, [idx, data[idx]])
      }
      assert(iter.next().done)
    })

    it('should give contentIdx from batchIdx', function() {
      const batchArray = ImmutBatchArray(data, batchSize)
      let batchIdx = batchArray.contentIdx(0, 1)
      assert.deepEqual(batchIdx, 1)
      batchIdx = batchArray.contentIdx(1, 2)
      assert.deepEqual(batchIdx, 6)
    })

    it('should give batchIdx from contentIdx', function() {
      const batchArray = ImmutBatchArray(data, batchSize)
      let batchIdx = batchArray.batchIdx(1)
      assert.deepEqual(batchIdx, [0, 1])
      batchIdx = batchArray.batchIdx(6)
      assert.deepEqual(batchIdx, [1, 2])
    })

    it('should set batchIdx to the end of a batch instead of beginning of next', function() {
      const batchArray = ImmutBatchArray(data, batchSize)
      let batchIdx = batchArray.batchIdx(3)
      assert.deepEqual(batchIdx, [0, 3])
      batchIdx = batchArray.batchIdx(7)
      assert.deepEqual(batchIdx, [1, 3])
    })

    it('should give nice batchIdxs from contentIdxs', function() {
      const batchArray = ImmutBatchArray(data, batchSize)
      let batchIdxs = batchArray.batchIdxs(0, 3)
      assert.deepEqual(batchIdxs, [[0, 0], [0, 3]])
      batchIdxs = batchArray.batchIdxs(3, 6)
      assert.deepEqual(batchIdxs, [[0, 3], [1, 2]])
    })

  })

  describe('array core', function() {

    it('has index access', function() {
      const batchArray = ImmutBatchArray(data, batchSize)
      let val = batchArray[7]
      assert.equal(val, 7)
      batchArray[7] = 14
      val = batchArray[7]
      assert.equal(val, 14)
      val = batchArray[11]
      assert.equal(val, undefined)
    })

    it('find', function() {
      const batchArray = ImmutBatchArray(data, batchSize)
      let foundVal = batchArray.find(val => (val - 5 === 2))
      assert.equal(foundVal, 7)
      foundVal = batchArray.find((_, index) => (index === 7))
      assert.equal(foundVal, 7)
      foundVal = batchArray.find((_1, index, obj) => (obj[index] - 5 === 2))
      assert.equal(foundVal, 7)
      foundVal = batchArray.find(val => (val === 11))
      assert.equal(foundVal, undefined)
    })

    it('findIndex', function() {
      const batchArray = ImmutBatchArray(data, batchSize)
      let foundVal = batchArray.findIndex(val => (val - 5 === 2))
      assert.equal(foundVal, 7)
      foundVal = batchArray.findIndex((_, index) => (index === 7))
      assert.equal(foundVal, 7)
      foundVal = batchArray.findIndex((_1, index, obj) => (obj[index] - 5 === 2))
      assert.equal(foundVal, 7)
      foundVal = batchArray.findIndex(val => (val === 11))
      assert.equal(foundVal, -1)
    })

    it('fill', function() {
      const batchArray = ImmutBatchArray(data, batchSize)
      batchArray.fill(17, 2, 9)
      const expected = [
        [0, 1, 17, 17],
        [17, 17, 17, 17],
        [17, 9, 10]
      ]
      assert.deepEqual(batchArray.rawBatches, expected)
    })

    it('copyWithin', function() {
      let batchArray = ImmutBatchArray(data, batchSize)
      batchArray.copyWithin(2, 4, 7)
      let expected = [
        [0, 1, 4, 5],
        [6, 5, 6, 7],
        [8, 9, 10]
      ]
      assert.deepEqual(batchArray.rawBatches, expected)
      batchArray = ImmutBatchArray(data, batchSize)
      batchArray.copyWithin(5, 4, 7)
      expected = [
        [0, 1, 2, 3],
        [4, 4, 5, 6],
        [8, 9, 10]
      ]
      assert.deepEqual(batchArray.rawBatches, expected)
      batchArray = ImmutBatchArray(data, batchSize)
      batchArray.copyWithin(9, 4, 7)
      expected = [
        [0, 1, 2, 3],
        [4, 5, 6, 7],
        [8, 4, 5]
      ]
      assert.deepEqual(batchArray.rawBatches, expected)
      batchArray = ImmutBatchArray(data, batchSize)
      batchArray.copyWithin(2, 9)
      expected = [
        [0, 1, 9, 10],
        [4, 5, 6, 7],
        [8, 9, 10]
      ]
      assert.deepEqual(batchArray.rawBatches, expected)
    })

  })

  describe('array iterable', function() {

    // Adjust data just so key and val are different
    const adjustedData = data.map(val => val + 1)

    it('iterate', function() {
      const batchArray = ImmutBatchArray(adjustedData, batchSize)
      let i = 0
      for (const item of batchArray) {
        assert.equal(item, i+1)
        ++i
      }
    })

    it('entries', function() {
      const batchArray = ImmutBatchArray(adjustedData, batchSize)
      let i = 0
      for (const [key, val] of batchArray.entries()) {
        assert.equal(key, i)
        assert.equal(val, i+1)
        ++i
      }
    })

    it('key', function() {
      const batchArray = ImmutBatchArray(adjustedData, batchSize)
      let i = 0
      for (const key of batchArray.keys()) {
        assert.equal(key, i)
        ++i
      }
    })

    it('entries', function() {
      const batchArray = ImmutBatchArray(adjustedData, batchSize)
      let i = 0
      for (const val of batchArray.values()) {
        assert.equal(val, i+1)
        ++i
      }
    })

  })

  describe('include', function() {

    it('includes', function() {
      const batchArray = ImmutBatchArray(data, batchSize)
      for (let i = 0; i < 11; ++i) assert(batchArray.includes(i))
      assert(!batchArray.includes(11))
      assert(!batchArray.includes(-1))
    })

  })

  describe('es5', function() {

    it('length', function() {
      const batchArray = ImmutBatchArray(data, batchSize)
      assert.equal(batchArray.length, 11)
    })

    it('toString', function() {
      const batchArray = ImmutBatchArray(data, batchSize)
      assert.equal(batchArray.toString(), '0,1,2,3,4,5,6,7,8,9,10')
      assert.equal(batchArray.toLocaleString(), '0,1,2,3,4,5,6,7,8,9,10')
    })

    it('concat', function() {
      const batchArray0 = ImmutBatchArray(data, batchSize)
      const adjustedData = data.map(val => -val)
      const batchArray1 = ImmutBatchArray(adjustedData, batchSize)
      const concatted = batchArray0.concat(batchArray1)
      const expected = [
        [0, 1, 2, 3],
        [4, 5, 6, 7],
        [8, 9, 10],
        [-0, -1, -2, -3],
        [-4, -5, -6, -7],
        [-8, -9, -10]
      ]
      assert.deepEqual(concatted.rawBatches, expected)
    })

    it('join', function() {
      const batchArray = ImmutBatchArray(data, batchSize)
      assert.equal(batchArray.join('-'), '0-1-2-3-4-5-6-7-8-9-10')
      assert.equal(batchArray.join(), '0,1,2,3,4,5,6,7,8,9,10')
    })

    it('reverse', function() {
      const batchArray = ImmutBatchArray(data, batchSize)
      const revedArray = batchArray.reverse()
      const expected = [
        [10, 9, 8],
        [7, 6, 5, 4],
        [3, 2, 1, 0]
      ]
      assert.deepEqual(revedArray.rawBatches, expected)
    })

    it('slice', function() {
      const batchArray = ImmutBatchArray(data, batchSize)
      let slice = batchArray.slice()
      let expected = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
      assert.deepEqual(slice, expected)
      slice = batchArray.slice(3, 7)
      expected = [3, 4, 5, 6]
      assert.deepEqual(slice, expected)
      slice = batchArray.slice(-4, 7)
      expected = [0, 1, 2, 3, 4, 5, 6]
      assert.deepEqual(slice, expected)
      slice = batchArray.slice(3, 12)
      expected = [3, 4, 5, 6, 7, 8, 9, 10]
      assert.deepEqual(slice, expected)
      slice = batchArray.slice(11, 12)
      expected = []
      assert.deepEqual(slice, expected)
    })

    it('sort', function() {
      const batchArray = ImmutBatchArray(data, batchSize)
      batchArray.sort((val1, val2) => val1 - val2)
      let expected = [
        [0, 1, 2, 3],
        [4, 5, 6, 7],
        [8, 9, 10]
      ]
      assert.deepEqual(batchArray.rawBatches, expected)
      batchArray.sort((val1, val2) => val2 - val1)
      expected = [
        [10, 9, 8, 7],
        [6, 5, 4, 3],
        [2, 1, 0]
      ]
      assert.deepEqual(batchArray.rawBatches, expected)
      batchArray.sort()
      expected = [
        [0, 1, 10, 2],
        [3, 4, 5, 6],
        [7, 8, 9]
      ]
      assert.deepEqual(batchArray.rawBatches, expected)
    })

    it('indexOf', function() {
      const batchArray = ImmutBatchArray(data, batchSize)
      batchArray[7] = 5
      assert.equal(batchArray.indexOf(5), 5)
      assert.equal(batchArray.indexOf(5, 4), 5)
      assert.equal(batchArray.indexOf(5, 6), 7)
      assert.equal(batchArray.indexOf(5, 8), -1)
      assert.equal(batchArray.indexOf(10, 6), 10)
      assert.equal(batchArray.indexOf(11), -1)
      assert.equal(batchArray.indexOf(11, 6), -1)
    })

    it('lastIndexOf', function() {
      const batchArray = ImmutBatchArray(data, batchSize)
      batchArray[7] = 5
      assert.equal(batchArray.lastIndexOf(5), 7)
      assert.equal(batchArray.lastIndexOf(5, 4), -1)
      assert.equal(batchArray.lastIndexOf(5, 6), 5)
      assert.equal(batchArray.lastIndexOf(5, 8), 7)
      assert.equal(batchArray.lastIndexOf(10, 6), -1)
      assert.equal(batchArray.lastIndexOf(11), -1)
      assert.equal(batchArray.lastIndexOf(11, 6), -1)
    })

    it('every/some', function() {
      const batchArray = ImmutBatchArray(data, batchSize)
      assert(batchArray.every(val => val >= 0))
      assert(!batchArray.every(val => val % 2 === 0))
      assert(batchArray.some(val => val >= 0))
      assert(batchArray.some(val => val % 2 === 0))
      assert(!batchArray.some(val => val < 0))
    })

    it('forEach', function() {
      const batchArray = ImmutBatchArray(data, batchSize)
      const output: number[] = []
      batchArray.forEach(val => { output.push(val*2) })
      const expected = [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20]
      assert.deepEqual(output, expected)
    })

    it('map', function() {
      const batchArray = ImmutBatchArray(data, batchSize)
      const expected = [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20]
      assert.deepEqual(batchArray.map(val => val * 2), expected)
    })

    it('filter', function() {
      const batchArray = ImmutBatchArray(data, batchSize)
      const expected = [0, 2, 4, 6, 8, 10]
      assert.deepEqual(batchArray.filter(val => val % 2 === 0), expected)
    })

    it('reduce', function() {
      const batchArray = ImmutBatchArray(data, batchSize)
      const output = batchArray.reduce((acc, val) => {
        acc.push(val % 3)
        return acc
      }, [] as number[])
      const expected = [0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 1]
      assert.deepEqual(output, expected)
      const sum = batchArray.reduce((acc, val) => acc += val, 0)
      assert.equal(sum, (10 * 11) / 2)
    })

    it('reduceRight', function() {
      const batchArray = ImmutBatchArray(data, batchSize)
      const output = batchArray.reduceRight((acc, val) => {
        acc.push(val % 3)
        return acc
      }, [] as number[])
      const expected = [1, 0, 2, 1, 0, 2, 1, 0, 2, 1, 0]
      assert.deepEqual(output, expected)
      const sum = batchArray.reduceRight((acc, val) => acc += val, 0)
      assert.equal(sum, (10 * 11) / 2)
    })

  })

})

describe('ImmutBatchArray', function() {

  const batchSize = 5
  const data = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

  it('throws', function() {
    const batchArray = ImmutBatchArray(data, batchSize)
    assert.throws(() => { batchArray.filterInPlace() })
    assert.throws(() => { batchArray.spliceAll() })
    assert.throws(() => { batchArray.pop() })
    assert.throws(() => { batchArray.push() })
    assert.throws(() => { batchArray.shift() })
    assert.throws(() => { batchArray.splice() })
    assert.throws(() => { batchArray.unshift() })
  })

})
