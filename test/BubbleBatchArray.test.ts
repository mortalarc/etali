import { strict as assert } from 'assert'
import 'mocha'
import { BubbleBatchArray } from '../src'


describe('BubbleBatchArray', function() {

  const maxBatchSize = 5
  const minBatchSize = 2
  const optimalSize = 4
  const data = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

  describe('misc', function() {

    it('should batch to "optimal" size', function() {
      const batchArray = BubbleBatchArray(data, maxBatchSize, minBatchSize)
      assert(batchArray.rawBatches.every(batch => batch.length <= optimalSize))
      const expected = [
        [0, 1, 2, 3],
        [4, 5, 6, 7],
        [8, 9, 10]
      ]
      assert.deepEqual(batchArray.rawBatches, expected)
    })

    it('should correctly filter in place', function() {
      const batchArray = BubbleBatchArray(data, maxBatchSize, minBatchSize)
      batchArray.filterInPlace(num => num % 3 != 0)
      const expected = [
        [1, 2],
        [4, 5, 7],
        [8, 10]
      ]
      assert.deepEqual(batchArray.rawBatches, expected)
    })

    it('has index access', function() {
      const batchArray = BubbleBatchArray(data, maxBatchSize, minBatchSize)
      let val = batchArray[7]
      assert.equal(val, 7)
      batchArray[7] = 14
      val = batchArray[7]
      assert.equal(val, 14)
      val = batchArray[11]
      assert.equal(val, undefined)
    })

    it('can be empty', function() {
      assert.doesNotThrow(() => {
        const batchArray = BubbleBatchArray<number>([], maxBatchSize, minBatchSize)
        batchArray.push(0)
        batchArray.pop()
        batchArray.pop()
        batchArray.push(0, 1, 2, 3, 4, 5, 6, 7, 8, 9)
      })
    })

  })

  describe('splice', function() {

    it('should remove items neatly', function() {
      let batchArray = BubbleBatchArray(data, maxBatchSize, minBatchSize)
      batchArray.splice(4, 3)
      let expected = [
        [0, 1, 2, 3],
        [7],
        [8, 9, 10]
      ]
      assert.deepEqual(batchArray.rawBatches, expected)
      batchArray = BubbleBatchArray(data, maxBatchSize, minBatchSize)
      batchArray.splice(8, 3)
      expected = [
        [0, 1, 2, 3],
        [4, 5, 6, 7],
        []
      ]
      assert.deepEqual(batchArray.rawBatches, expected)
    })

    it('should add items neatly', function() {
      const batchArray = BubbleBatchArray(data, maxBatchSize, minBatchSize)
      batchArray.splice(3, 0, ...[2.1])
      const expected = [
        [0, 1, 2, 2.1, 3],
        [4, 5, 6, 7],
        [8, 9, 10]
      ]
      assert.deepEqual(batchArray.rawBatches, expected)
    })

    it('should split added items evenly between batches', function() {
      const batchArray = BubbleBatchArray(data, maxBatchSize, minBatchSize)
      batchArray.splice(4, 0, ...[3.1, 3.2])
      const expected = [
        [0, 1, 2, 3, 3.1],
        [3.2, 4, 5, 6, 7],
        [8, 9, 10]
      ]
      assert.deepEqual(batchArray.rawBatches, expected)
    })

    it('should split more items on first batch if uneven', function() {
      const batchArray = BubbleBatchArray(data, 6, minBatchSize)
      batchArray.splice(4, 0, ...[3.1, 3.2, 3.3])
      const expected = [
        [0, 1, 2, 3, 3.1, 3.2],
        [3.3, 4, 5, 6, 7],
        [8, 9, 10]
      ]
      assert.deepEqual(batchArray.rawBatches, expected)
    })

    it('should add and remove items consistently', function() {
      const batchArray = BubbleBatchArray(data, maxBatchSize, minBatchSize)
      batchArray.splice(3, 3, ...[2.1, 2.2])
      const expected = [
        [0, 1, 2, 2.1],
        [2.2, 6, 7],
        [8, 9, 10]
      ]
      assert.deepEqual(batchArray.rawBatches, expected)
    })

    it('should be able to split additions between three batches', function() {
      const batchArray = BubbleBatchArray(data, maxBatchSize, minBatchSize)
      batchArray.splice(2, 7, ...[3.1, 3.2, 3.3, 3.4, 3.5, 3.6])
      const expected = [
        [0, 1, 3.1, 3.2],
        [3.3, 3.4],
        [3.5, 3.6, 9, 10]
      ]
      assert.deepEqual(batchArray.rawBatches, expected)
    })

  })

  describe('mergeSmallBatches', function() {

    it('should merge when neighbor batches are too small', function() {
      const batchArray = BubbleBatchArray(data, maxBatchSize, minBatchSize)
      batchArray.splice(1, 6)
      const expected = [
        [0, 7],
        [8, 9, 10]
      ]
      assert.deepEqual(batchArray.rawBatches, expected)
    })

    it('can merge more than two neighbor batches', function() {
      const batchArray = BubbleBatchArray(data, maxBatchSize, minBatchSize)
      batchArray.spliceAll([1, 6], [8, 2])
      const expected = [
        [0, 7, 10]
      ]
      assert.deepEqual(batchArray.rawBatches, expected)
    })

  })

  describe('splitLargeBatches', function() {

    it('should split large batches into neighbors', function() {
      const batchArray = BubbleBatchArray(data, maxBatchSize, minBatchSize)
      batchArray.splice(1, 0, ...[0.1, 0.2, 0.3])
      const expected = [
        [0, 0.1, 0.2, 0.3],
        [1, 2, 3],
        [4, 5, 6, 7],
        [8, 9, 10]
      ]
      assert.deepEqual(batchArray.rawBatches, expected)
    })

    it('should split into more than two if necessary', function() {
      const batchArray = BubbleBatchArray(data, maxBatchSize, minBatchSize)
      batchArray.spliceAll(
        [1, 0, ...[0.1, 0.2, 0.3]],
        [2, 0, ...[1.1, 1.2, 1.3]]
      )
      const expected = [
        [0, 0.1, 0.2, 0.3],
        [1, 1.1, 1.2],
        [1.3, 2, 3],
        [4, 5, 6, 7],
        [8, 9, 10]
      ]
      assert.deepEqual(batchArray.rawBatches, expected)
    })

  })

  describe('array methods', function() {

    it('pop', function() {
      const batchArray = BubbleBatchArray(data, maxBatchSize, minBatchSize)
      assert.equal(batchArray.rawBatches.length, 3)
      let poppedNum = batchArray.pop()
      assert.equal(poppedNum, 10)
      assert.equal(batchArray.length, 10)
      assert.equal(batchArray.rawBatches.length, 3)
      poppedNum = batchArray.pop()
      assert.equal(poppedNum, 9)
      assert.equal(batchArray.length, 9)
      assert.equal(batchArray.rawBatches.length, 3)
      poppedNum = batchArray.pop()
      assert.equal(poppedNum, 8)
      assert.equal(batchArray.length, 8)
      assert.equal(batchArray.rawBatches.length, 2)
      for (let idx = 7; idx > 0; --idx) batchArray.pop()
      assert.equal(batchArray.length, 1)
      assert.equal(batchArray.rawBatches.length, 1)
      poppedNum = batchArray.pop()
      assert.equal(poppedNum, 0)
      assert.equal(batchArray.length, 0)
      assert.equal(batchArray.rawBatches.length, 0)
      poppedNum = batchArray.pop()
      assert.equal(poppedNum, undefined)
      assert.equal(batchArray.length, 0)
      assert.equal(batchArray.rawBatches.length, 0)
    })

    it('push', function() {
      const batchArray = BubbleBatchArray(data, maxBatchSize, minBatchSize)
      let idx = 10
      assert.equal(batchArray.rawBatches.length, 3)
      let pushedNum = batchArray.push(++idx)
      assert.equal(pushedNum, 12)
      assert.equal(batchArray.length, 12)
      assert.equal(batchArray.rawBatches.length, 3)
      pushedNum = batchArray.push(++idx)
      assert.equal(pushedNum, 13)
      assert.equal(batchArray.length, 13)
      assert.equal(batchArray.rawBatches.length, 4)
      pushedNum = batchArray.push(++idx)
      assert.equal(pushedNum, 14)
      assert.equal(batchArray.length, 14)
      assert.equal(batchArray.rawBatches.length, 4)
      pushedNum = batchArray.push(++idx)
      assert.equal(pushedNum, 15)
      assert.equal(batchArray.length, 15)
      assert.equal(batchArray.rawBatches.length, 4)
      let expected = [
        [0, 1, 2, 3],
        [4, 5, 6, 7],
        [8, 9, 10, 11],
        [12, 13, 14]
      ]
      assert.deepEqual(batchArray.rawBatches, expected)
      batchArray.push(++idx, ++idx, ++idx, ++idx, ++idx)
      expected = [
        [0, 1, 2, 3],
        [4, 5, 6, 7],
        [8, 9, 10, 11],
        [12, 13, 14, 15],
        [16, 17, 18, 19]
      ]
      assert.deepEqual(batchArray.rawBatches, expected)
    })

    it('pop/push', function() {
      const batchArray = BubbleBatchArray(data, maxBatchSize, minBatchSize)
      let expected = [
        [0, 1, 2, 3],
        [4, 5, 6, 7],
        [8, 9, 10]
      ]
      assert.deepEqual(batchArray.rawBatches, expected)
      batchArray.pop()
      batchArray.pop()
      batchArray.pop()
      expected = [
        [0, 1, 2, 3],
        [4, 5, 6, 7]
      ]
      assert.deepEqual(batchArray.rawBatches, expected)
      batchArray.push(8, 9)
      expected = [
        [0, 1, 2, 3],
        [4, 5, 6, 7],
        [8, 9]
      ]
      assert.deepEqual(batchArray.rawBatches, expected)
    })

    it('shift', function() {
      const batchArray = BubbleBatchArray(data, maxBatchSize, minBatchSize)
      assert.equal(batchArray.rawBatches.length, 3)
      let shiftedNum = batchArray.shift()
      assert.equal(shiftedNum, 0)
      assert.equal(batchArray.length, 10)
      assert.equal(batchArray.rawBatches.length, 3)
      shiftedNum = batchArray.shift()
      assert.equal(shiftedNum, 1)
      assert.equal(batchArray.length, 9)
      assert.equal(batchArray.rawBatches.length, 3)
      shiftedNum = batchArray.shift()
      assert.equal(shiftedNum, 2)
      assert.equal(batchArray.length, 8)
      assert.equal(batchArray.rawBatches.length, 3)
      shiftedNum = batchArray.shift()
      assert.equal(shiftedNum, 3)
      assert.equal(batchArray.length, 7)
      assert.equal(batchArray.rawBatches.length, 2)
      for (let idx = 4; idx < 10; ++idx) batchArray.shift()
      assert.equal(batchArray.length, 1)
      assert.equal(batchArray.rawBatches.length, 1)
      shiftedNum = batchArray.shift()
      assert.equal(shiftedNum, 10)
      assert.equal(batchArray.length, 0)
      assert.equal(batchArray.rawBatches.length, 0)
      shiftedNum = batchArray.shift()
      assert.equal(shiftedNum, undefined)
      assert.equal(batchArray.length, 0)
      assert.equal(batchArray.rawBatches.length, 0)
    })

    it('unshift', function() {
      const batchArray = BubbleBatchArray(data, maxBatchSize, minBatchSize)
      let idx = 0
      assert.equal(batchArray.rawBatches.length, 3)
      let unshiftedNum = batchArray.unshift(--idx)
      assert.equal(unshiftedNum, 12)
      assert.equal(batchArray.length, 12)
      assert.equal(batchArray.rawBatches.length, 4)
      unshiftedNum = batchArray.unshift(--idx)
      assert.equal(unshiftedNum, 13)
      assert.equal(batchArray.length, 13)
      assert.equal(batchArray.rawBatches.length, 4)
      unshiftedNum = batchArray.unshift(--idx)
      assert.equal(unshiftedNum, 14)
      assert.equal(batchArray.length, 14)
      assert.equal(batchArray.rawBatches.length, 4)
      unshiftedNum = batchArray.unshift(--idx)
      assert.equal(unshiftedNum, 15)
      assert.equal(batchArray.length, 15)
      assert.equal(batchArray.rawBatches.length, 4)
      let expected = [
        [-4, -3, -2, -1],
        [0, 1, 2, 3],
        [4, 5, 6, 7],
        [8, 9, 10]
      ]
      assert.deepEqual(batchArray.rawBatches, expected)
      batchArray.unshift(...[--idx, --idx, --idx, --idx, --idx].reverse())
      expected = [
        [-9],
        [-8, -7, -6, -5],
        [-4, -3, -2, -1],
        [0, 1, 2, 3],
        [4, 5, 6, 7],
        [8, 9, 10]
      ]
      assert.deepEqual(batchArray.rawBatches, expected)
    })

    it('shift/unshift', function() {
      const batchArray = BubbleBatchArray(data, maxBatchSize, minBatchSize)
      let expected = [
        [0, 1, 2, 3],
        [4, 5, 6, 7],
        [8, 9, 10]
      ]
      assert.deepEqual(batchArray.rawBatches, expected)
      batchArray.shift()
      batchArray.shift()
      batchArray.shift()
      batchArray.shift()
      expected = [
        [4, 5, 6, 7],
        [8, 9, 10]
      ]
      assert.deepEqual(batchArray.rawBatches, expected)
      batchArray.unshift(2, 3)
      expected = [
        [2, 3],
        [4, 5, 6, 7],
        [8, 9, 10]
      ]
      assert.deepEqual(batchArray.rawBatches, expected)
    })

  })

})
